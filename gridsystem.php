<?php

header("content-type: text/css; charset: UTF-8");
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

$CL = 18;   // qualidade total de colunas
$SP = 7.5;  // padding esquerdo direito da coluna
$CD = 6;    // casas decimais da percentagem.

$grid = [   // configuração do grid
    'xs' => 0,
    'sm' => 576,
    'md' => 768,
    'lg' => 992,
    'xl' => 1200,
];

function percentagem(float $number, int $precision = 6): string{
    return (($p = ((float) number_format(($number * 100), $precision, '.', ''))) . ((!$p)? '': '%'));
}

function pixel(float $number, int $precision = 6): string{
    return (($p = ((float) number_format(($number), $precision, '.', ''))) . ((!$p)? '': 'px'));
}

ob_start(); ?>
/**
 * Grid System
 */

.container-plr:before, .container-plr:after,
.container-pl:before,  .container-pl:after,
.container-pr:before,  .container-pr:after
{ display: table; content: " " }

.container-plr:after, .container-pl:after, .container-pr:after { clear: both }
.container-plr, .container-pl { padding-left: <?php echo pixel($SP); ?> }
.container-plr, .container-pr { padding-right: <?php echo pixel($SP); ?> }

.card-padding{ --card-padding: 1.25em; padding: 0 var(--card-padding); }
.card-padding:before, .card-padding:after{ content: ''; display: table; clear: both;}
.card-padding:before{ margin-bottom: var(--card-padding) }
.card-padding:after{ margin-top: var(--card-padding) }

.row {
    display: flex;
    flex-wrap: wrap;
    margin-right: <?php echo pixel(($SP * -1)); ?>;
    margin-left: <?php echo pixel(($SP * -1)); ?>;
}

.flex-direction-row{ flex-direction: row !important }
.flex-direction-column{ flex-direction: column !important }
.flex-direction-row-reverse{ flex-direction: row-reverse !important }
.flex-direction-column-reverse{ flex-direction: column-reverse !important }

.flex-wrap-wrap{ flex-wrap: wrap !important }
.flex-wrap-nowrap{ flex-wrap: nowrap !important }
.flex-wrap-wrap-reverse{ flex-wrap: wrap-reverse !important }

.justify-content-start { justify-content: flex-start !important }
.justify-content-end { justify-content: flex-end !important }
.justify-content-center { justify-content: center !important }
.justify-content-between { justify-content: space-between !important }
.justify-content-around { justify-content: space-around !important }

.align-items-start { align-items: flex-start !important; }
.align-items-end { align-items: flex-end !important; }
.align-items-center { align-items: center !important; }
.align-items-baseline { align-items: baseline !important; }
.align-items-stretch { align-items: stretch !important; }

.align-content-start { align-content: flex-start !important; }
.align-content-end { align-content: flex-end !important; }
.align-content-center { align-content: center !important; }
.align-content-between { align-content: space-between !important; }
.align-content-around { align-content: space-around !important; }
.align-content-stretch { align-content: stretch !important; }

<?php foreach($grid as $p => $w): if($w > 0): ?>

@media (min-width: <?php echo $w; ?>px) {
    .container-plr, .container-pl { padding-left:  calc(((100vw - <?php echo pixel($w); ?>) / 2) + <?php echo pixel($SP); ?>); }
    .container-plr, .container-pr { padding-right: calc(((100vw - <?php echo pixel($w); ?>) / 2) + <?php echo pixel($SP); ?>); }

    .<?php echo $p; ?>-flex-direction-row{ flex-direction: row !important }
    .<?php echo $p; ?>-flex-direction-column{ flex-direction: column !important }
    .<?php echo $p; ?>-flex-direction-row-reverse{ flex-direction: row-reverse !important }
    .<?php echo $p; ?>-flex-direction-column-reverse{ flex-direction: column-reverse !important }

    .<?php echo $p; ?>-flex-wrap-wrap{ flex-wrap: wrap !important }
    .<?php echo $p; ?>-flex-wrap-nowrap{ flex-wrap: nowrap !important }
    .<?php echo $p; ?>-flex-wrap-wrap-reverse{ flex-wrap: wrap-reverse !important }

    .<?php echo $p; ?>-justify-content-start { justify-content: flex-start !important }
    .<?php echo $p; ?>-justify-content-end { justify-content: flex-end !important }
    .<?php echo $p; ?>-justify-content-center { justify-content: center !important }
    .<?php echo $p; ?>-justify-content-between { justify-content: space-between !important }
    .<?php echo $p; ?>-justify-content-around { justify-content: space-around !important }

    .<?php echo $p; ?>-align-items-start { align-items: flex-start !important; }
    .<?php echo $p; ?>-align-items-end { align-items: flex-end !important; }
    .<?php echo $p; ?>-align-items-center { align-items: center !important; }
    .<?php echo $p; ?>-align-items-baseline { align-items: baseline !important; }
    .<?php echo $p; ?>-align-items-stretch { align-items: stretch !important; }

    .<?php echo $p; ?>-align-content-start { align-content: flex-start !important; }
    .<?php echo $p; ?>-align-content-end { align-content: flex-end !important; }
    .<?php echo $p; ?>-align-content-center { align-content: center !important; }
    .<?php echo $p; ?>-align-content-between { align-content: space-between !important; }
    .<?php echo $p; ?>-align-content-around { align-content: space-around !important; }
    .<?php echo $p; ?>-align-content-stretch { align-content: stretch !important; }
}
<?php endif; endforeach; ?>


<?php $C = []; foreach($grid as $p => $w): for($i = 1; $i <= $CL; $i++): for($j = 1; $j <= $i; $j++): $C[] = ".$p-$j-$i"; endfor; endfor; endforeach; echo implode(',', $C); unset($C); ?>{
    position: relative;
    width: 100%;
    flex: 0 0 100%;
    max-width: 100%;
    padding-right: <?php echo pixel($SP); ?>;
    padding-left: <?php echo pixel($SP); ?>;
}


<?php foreach($grid as $p => $w): if(!!$w): ?>

@media (min-width: <?php echo pixel($w); ?>) {
<?php endif; for($i = 1; $i <= $CL; $i++): for($j = 1; $j <= $i; $j++): $por = ($j / $i);
if(!!$w): ?>    <?php endif; ?>.<?php echo "$p-$j-$i"; ?>{ flex: 0 0 <?php echo $t = percentagem($por, $CD); ?>; max-width: <?php echo $t; ?> }
<?php endfor; endfor; ?>
<?php if(!!$w): ?>}<?php endif; endforeach; ?>


<?php foreach($grid as $p => $w): if(!!$w): ?>

@media (min-width: <?php echo pixel($w); ?>) {
<?php endif;
if(!!$w): ?>    <?php endif; ?>.<?php echo "$p-wa"; ?>{ flex: 0 0 auto; width: auto; max-width: 100% }
<?php if(!!$w): ?>}<?php endif; endforeach; ?>


<?php foreach($grid as $p => $w): if(!!$w): ?>

@media (min-width: <?php echo pixel($w); ?>) {
<?php endif; for($i = 1; $i <= $CL; $i++): for($j = 0; $j < $i; $j++): $por = ($j / $i); ?>
<?php if(!!$w): ?>    <?php endif; ?>.<?php echo "$p-o-$j-$i"; ?>,.<?php echo "$p-offset-$j-$i"; ?>{ margin-left: <?php echo percentagem($por, $CD); ?> }
<?php endfor; endfor; if(!!$w): ?>}<?php endif; endforeach;


$css = ob_get_clean();
echo $css;

file_put_contents(dirname(__FILE__). '/gridsystem.css', $css);

$css = str_replace(["\n", "    ", "{ ", " }", "; "], ['', '', '{', '}', ';'], preg_replace('/\/\*.*\*\//Us', '', $css));

file_put_contents(dirname(__FILE__). '/gridsystem.min.css', $css);